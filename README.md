# Parallel DAG Partition

HPC Final Project

Note that this project is based on Opentimer[https://github.com/OpenTimer/OpenTimer.git]. We modified the core method update_timing in Opentimer.

Our source codes are mostly written in ot/timer/timer.cpp and ot/timer/partition_gpu.cu. 

To build the project:

First we need to git clone the moderngpu library from https://github.com/moderngpu/moderngpu into ot/timer/
```
# in root of our project
git clone https://github.com/moderngpu/moderngpu
mv moderngpu ot/timer

```

Then we can build it:

```
mkdir build
cd build
cmake ../
make   
         
```

To test the project (after build):

```
cd ../ # back to root of our project
cd benchmark
cd simple # choose any circuit you would like to check
../../bin/ot-shell < simple.conf

```

Note that the simple.conf looks like this:
```
set_early_celllib_fpath simple_Early.lib
set_late_celllib_fpath simple_Late.lib
set_verilog_fpath simple.v
read_spef simple.spef
read_sdc simple.sdc
update_timing

```

To try other circuit benchmark, we need to create a corresponding .conf file(which is not originally provided by Opentimer).
For example, to check circuit c17, we need to create a c17.conf that looks like this:
```
set_early_celllib_fpath c17_Early.lib
set_late_celllib_fpath c17_Late.lib
set_verilog_fpath c17.v
read_spef c17.spef
read_sdc c17.sdc
update_timing

```
which is changing all the "simple" to "c17". Same rule can be applied to generate other .conf file for other circuit to check.

After running ../../bin/ot-shell < simple.conf, the output looks like this:
```
W 32768 23-12-12 01:08:31 obselete.cpp:17] "set_early_celllib_fpath" is obselete; use "read_celllib"
W 32768 23-12-12 01:08:31 obselete.cpp:27] "set_late_celllib_fpath" is obselete; use "read_celllib"
W 32768 23-12-12 01:08:31 obselete.cpp:37] "set_verilog_fpath" is obselete; use "read_verilog"
I 53248 23-12-12 01:08:31 celllib.cpp:34] loading celllib "simple_Early.lib"
I 57344 23-12-12 01:08:31 verilog.cpp:14] loading netlist "simple.v"
I 61440 23-12-12 01:08:31 celllib.cpp:34] loading celllib "simple_Late.lib"
I 53248 23-12-12 01:08:31 spef.cpp:15] loading spef "simple.spef"
I 36864 23-12-12 01:08:31 sdc.cpp:35] loading sdc "simple.sdc" ...
I 53248 23-12-12 01:08:31 unit.cpp:244] use celllib time unit 1e-12 s
I 53248 23-12-12 01:08:31 unit.cpp:258] use celllib capacitance unit 1e-15 F
I 53248 23-12-12 01:08:31 unit.cpp:272] use celllib current unit 0.001 A
I 53248 23-12-12 01:08:31 unit.cpp:286] use celllib voltage unit 1 V
I 53248 23-12-12 01:08:31 unit.cpp:300] use celllib resistance unit 1000 Ohm
I 53248 23-12-12 01:08:31 unit.cpp:314] use celllib power unit 1e-06 W
I 53248 23-12-12 01:08:31 celllib.cpp:66] added min celllib "contest" [cells:211]
I 61440 23-12-12 01:08:31 celllib.cpp:66] added max celllib "contest" [cells:211]
I 61440 23-12-12 01:08:31 verilog.cpp:21] added verilog module "simple" [gates:5]
I 61440 23-12-12 01:08:31 spef.cpp:27] added 6 spef nets
I 61440 23-12-12 01:08:31 sdc.cpp:21] added 30 sdc commands
the size of _adjncy is 17
before partition, graph size: 17
after GDCA partition, graph size: 5
dependency amount: 4
digraph Taskflow {
subgraph cluster_p0x7ffc65d36390 {
label="Taskflow: p0x7ffc65d36340";
p0x55cd27b377c0[label="0" ];
p0x55cd27b377c0 -> p0x55cd27b376d0;
p0x55cd27b377c0 -> p0x55cd27b37040;
p0x55cd27b376d0[label="0" ];
p0x55cd27b376d0 -> p0x55cd27b375e0;
p0x55cd27b376d0 -> p0x55cd27b36e60;
p0x55cd27b375e0[label="0" ];
p0x55cd27b375e0 -> p0x55cd27b374f0;
p0x55cd27b374f0[label="0" ];
p0x55cd27b374f0 -> p0x55cd27b37400;
p0x55cd27b374f0 -> p0x55cd27b37220;
p0x55cd27b37400[label="0" ];
p0x55cd27b37400 -> p0x55cd27b37310;
p0x55cd27b37310[label="2" ];
p0x55cd27b37220[label="0" ];
p0x55cd27b37220 -> p0x55cd27b368c0;
p0x55cd27b368c0[label="3" ];
p0x55cd27b369b0[label="1" ];
p0x55cd27b369b0 -> p0x55cd27b36aa0;
p0x55cd27b36aa0[label="1" ];
p0x55cd27b36aa0 -> p0x55cd27b36b90;
p0x55cd27b36b90[label="1" ];
p0x55cd27b36b90 -> p0x55cd27b36c80;
p0x55cd27b36c80[label="1" ];
p0x55cd27b36c80 -> p0x55cd27b36d70;
p0x55cd27b36d70[label="1" ];
p0x55cd27b36d70 -> p0x55cd27b36f50;
p0x55cd27b36e60[label="0" ];
p0x55cd27b36e60 -> p0x55cd27b36f50;
p0x55cd27b36f50[label="1" ];
p0x55cd27b36f50 -> p0x55cd27b37040;
p0x55cd27b37040[label="1" ];
p0x55cd27b37040 -> p0x55cd27b37130;
p0x55cd27b37130[label="4" ];
}
}
GDCA_par_runtime: 0
CPU_par_runtime(sequential): 0
GPU_par_runtime(non-deterministic): 186
GPU_par_deter_runtime(deterministic): 0
_vivek_btask_rebuild_time: 0
_vivek_btask_runtime: 157

```

To visualize the partitioning result, you need to copy the following part from the output:
```
digraph Taskflow {
subgraph cluster_p0x7ffc65d36390 {
label="Taskflow: p0x7ffc65d36340";
p0x55cd27b377c0[label="0" ];
p0x55cd27b377c0 -> p0x55cd27b376d0;
p0x55cd27b377c0 -> p0x55cd27b37040;
p0x55cd27b376d0[label="0" ];
p0x55cd27b376d0 -> p0x55cd27b375e0;
p0x55cd27b376d0 -> p0x55cd27b36e60;
p0x55cd27b375e0[label="0" ];
p0x55cd27b375e0 -> p0x55cd27b374f0;
p0x55cd27b374f0[label="0" ];
p0x55cd27b374f0 -> p0x55cd27b37400;
p0x55cd27b374f0 -> p0x55cd27b37220;
p0x55cd27b37400[label="0" ];
p0x55cd27b37400 -> p0x55cd27b37310;
p0x55cd27b37310[label="2" ];
p0x55cd27b37220[label="0" ];
p0x55cd27b37220 -> p0x55cd27b368c0;
p0x55cd27b368c0[label="3" ];
p0x55cd27b369b0[label="1" ];
p0x55cd27b369b0 -> p0x55cd27b36aa0;
p0x55cd27b36aa0[label="1" ];
p0x55cd27b36aa0 -> p0x55cd27b36b90;
p0x55cd27b36b90[label="1" ];
p0x55cd27b36b90 -> p0x55cd27b36c80;
p0x55cd27b36c80[label="1" ];
p0x55cd27b36c80 -> p0x55cd27b36d70;
p0x55cd27b36d70[label="1" ];
p0x55cd27b36d70 -> p0x55cd27b36f50;
p0x55cd27b36e60[label="0" ];
p0x55cd27b36e60 -> p0x55cd27b36f50;
p0x55cd27b36f50[label="1" ];
p0x55cd27b36f50 -> p0x55cd27b37040;
p0x55cd27b37040[label="1" ];
p0x55cd27b37040 -> p0x55cd27b37130;
p0x55cd27b37130[label="4" ];
}
}

```

And paste it to https://dreampuf.github.io/GraphvizOnline/.

